import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http:HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
conertToShortUrl(url){ 
  return this.http.post('http://18.222.81.171:3000/api/url/shorten',{longUrl:url},this.httpOptions);
}

}
