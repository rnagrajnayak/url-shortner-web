import { Component } from '@angular/core';
import {AppService} from './app.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'url-shortner';
  public longUrl:any = '';
  public shortUrl:any;
  public urlData:any;
  showLink:boolean = false;


constructor(private appAervice:AppService){}

shortenLongUrl(){
  if(this.longUrl=== ''){
   alert('please enter a valid url');
  }
  else{
    this.appAervice.conertToShortUrl(this.longUrl).subscribe(
    (res:any) =>{
      if(res.status === 401){
        this.showLink = false;
        alert("please enter a valid url");
        
      }else{
        this.urlData = res;
        this.shortUrl = this.urlData.shortUrl;
        console.log("this is shorturl",this.shortUrl);
        if(this.shortUrl=='' || this.shortUrl==null || this.shortUrl=='undefined' ){
          alert("please enter a valid url");
          this.showLink = false;
        }else {
          this.showLink = true;
        }
      }

    },
    err =>{
      console.log(err);
    }
    )

  }

}


}
